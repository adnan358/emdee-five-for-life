import hashlib
import requests
from bs4 import BeautifulSoup


url='http://167.99.85.197:31681'

s=requests.Session()
r=s.get(url)
if r.status_code == 200:
    soup = BeautifulSoup(r.content, "html.parser")
    find=soup.find('h3')
    text=find.text

    h=hashlib.md5()
    h.update(text)
    ha=h.hexdigest()

    print(s.post(url, data={'hash':ha}).text)
