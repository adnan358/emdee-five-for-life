require 'httparty'
require 'nokogiri'
require 'digest'

url = 'http://134.209.29.219:30025'

unparsed = HTTParty.get(url)
parsed = Nokogiri::HTML(unparsed)
text = parsed.css('h3').text

md5 = Digest::MD5.hexdigest text


puts HTTParty.post(url, {
  body: "hash=#{md5}"
}).body
